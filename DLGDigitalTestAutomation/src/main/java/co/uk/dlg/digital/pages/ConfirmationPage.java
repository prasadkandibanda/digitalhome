package co.uk.dlg.digital.pages;

import com.usmanhussain.habanero.framework.AbstractPage;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

public class ConfirmationPage extends AbstractPage {

    public WebElement dlconfirmationText() {
        return waitAndFindElement(By.xpath("//h4[contains(text(),'Thank you')]"));
    }

    // in churchill and privilege confiramtion image is displayed.
    public WebElement confirmationImg() {
        return waitForElementPresent(By.xpath("//img[@alt='confirmation image']"));
    }


}
