package co.uk.dlg.digital.pages;

import com.usmanhussain.habanero.framework.AbstractPage;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

public class PortfolioPage extends AbstractPage {

    public WebElement portfolioLink() {
        return waitAndFindElement(By.xpath("//a[contains(text(),'portfolio')]"));
    }

    public WebElement changeEmailAddressLink() {
        return waitForElementPresent(By.id("change-email-address"));
    }

    public WebElement changePasswordLink() {
        return waitForElementPresent(By.id("change-password"));
    }

    public WebElement logoutLink() {
        return waitForElementPresent(By.id("qpm-logout"));
    }

    // After clicking portfolio link , Review & Buy button page
    public WebElement reviewAndBuyButton() {
        return waitForElementPresent(By.xpath("//*[@data-test-id='review-and-buy0']"));
    }

}
