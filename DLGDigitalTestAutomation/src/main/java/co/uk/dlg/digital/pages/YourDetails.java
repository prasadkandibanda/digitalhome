package co.uk.dlg.digital.pages;

import com.usmanhussain.habanero.framework.AbstractPage;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class YourDetails extends AbstractPage {

    @FindBy(xpath = "//*[@data-test-id='property-type'][text()='propData']")
    WebElement ClickElementType;

    public WebElement postCode() {
        return waitForElementPresent(By.id("postCode"));
    }

    public WebElement findMyAddr() {
        return waitForElementPresent(By.id("addressLookUp"));
    }

    public WebElement chooseAddr() {
        return waitForUnstableElement(By.xpath("//select[@id='chooseAddress']/option[3]"));
    }

    public WebElement ownerShip(String ownerType) {

        return waitForElementPresent(By.xpath("//*[@data-test-id='ownership'][text()='" + ownerType + "']"));
    }

    public WebElement coverType(String insuranceCover) {
        return waitForElementPresent(By.xpath("//*[@data-test-id='cover-level-group-label'][text()='" + insuranceCover + "']"));
    }

    public WebElement approxYrBuilt() {
        return waitForElementPresent(By.id("approx-year-built"));
    }

    public WebElement propertyType(String propertyType) {
        return waitForElementPresent(By.xpath("//*[@data-test-id='property-grp-label'][text()='" + propertyType + "']"));
    }

    public WebElement describeProperty(String describe) {
        return waitForElementPresent(By.xpath("//*[@data-test-id='property-type'][text()='" + describe + "']"));
    }

    public WebElement floodedNo() {
        return waitForUnstableElement(By.xpath(".//*[@data-test-id='flooded-property-false']"));
    }

    public WebElement extWallType(String wallType) {
        return waitForUnstableElement(By.xpath("//*[@data-test-id='exterior-walls'][text()='" + wallType + "']"));
    }

    public WebElement roofType(String roofType) {
        return waitForElementPresent(By.xpath("//*[@data-test-id='flat-roof'][text()='" + roofType + "']"));
    }

    public WebElement bedRooms(String bedRooms) {
        return waitForElementPresent(By.xpath("//*[@data-test-id='bedrooms'][text()='" + bedRooms + "']"));
    }

    public WebElement bathRooms(String bathRooms) {
        return waitForElementPresent(By.xpath("//*[@data-test-id='bathroom-toilets'][text()='" + bathRooms + "']"));
    }

    public WebElement adults(String adults) {
        return waitForElementPresent(By.xpath("//*[@data-test-id='nr-of-adults'][text()='" + adults + "']"));
    }

    public WebElement children(String children) {
        return waitForElementPresent(By.xpath("//*[@data-test-id='nr-of-children'][text()='" + children + "']"));
    }

    public WebElement coverStartMonthDropdown() {
        return waitForElementPresent(By.id("startDateMM"));
    }

    public WebElement coverStartDayDropdown() {
        return waitForElementPresent(By.id("startDateDD"));
    }

    public WebElement nextBtn() {
        return waitAndFindElement(By.linkText("Next"));
    }

    //--------------------------2. Tell us about your claims ----------------------------------------------
    public WebElement noOfClaims(String numberOfClaims) {
        return waitForUnstableElement(By.xpath("//*[@data-test-id='num-claims-dd'][text()='" + numberOfClaims + "']"));
    }

    public WebElement claimMonthSelect(int NoOfclaims) {
        return waitForUnstableElement(By.id("claimMonth" + NoOfclaims));
    }

    public WebElement claimYearSelect(int NoOfclaims) {
        return waitForUnstableElement(By.id("claimYear" + NoOfclaims));
    }

    public WebElement typeOfClaimSelect(int NoOfclaims) {
        return waitForUnstableElement(By.id("typeOfClaim" + NoOfclaims));
    }

    public WebElement claimValueEnter(int NoOfclaims) {
        return waitForUnstableElement(By.id(("claimValue" + NoOfclaims)));
    }

    public WebElement claimAddressYesButton(int i) {
        String datatestid = "wasAdress-of-claim-yes" + i;
        return waitForUnstableElement(By.xpath("//*[@data-test-id='" + datatestid + "']"));
    }

    public WebElement claimFreeYearsBuildings(String claimsOnBuilding) {
        return waitForUnstableElement(By.xpath("//*[@data-test-id='claim-free-buildings'][text()='" + claimsOnBuilding + "']"));
    }

    public WebElement claimFreeYearsContents(String claimsOnContents) {
        return waitForUnstableElement(By.xpath("//*[@data-test-id='claim-free-content'][text()='" + claimsOnContents + "']"));

    }

    //----------------------------------------3. Tell us about yourself --------------------------------------------------
    public WebElement title(String title) {
        return waitForUnstableElement(By.xpath("//*[@data-test-id='title'][text()='" + title + "']"));
    }

    public WebElement firstName() {
        return waitForUnstableElement(By.id("firstName"));
    }

    public WebElement lastName() {
        return waitForElementPresent(By.id("surname"));
    }

    public WebElement dateDOB() {
        return waitForElementPresent(By.id("dobDD"));
    }

    public WebElement monthDOB() {
        return waitForElementPresent(By.id("dobMM"));
    }

    public WebElement yearDOB() {
        return waitForElementPresent(By.id("dobYY"));
    }

    public WebElement email() {
        return waitForUnstableElement(By.id("emailAddress"));
    }

    public WebElement policyPaymentMethodAnnually() {
        return waitForUnstableElement(By.xpath("//*[@data-test-id='ppmethod-annual']"));
    }

    public WebElement policyPaymetMethodMonthly() {
        return waitForUnstableElement(By.xpath("//*[@data-test-id='ppmethod-monthly']"));
    }

    //---------------------------------------6. Important information  -------------------------------------------------------------
    public WebElement impInfoChkBox() {
        return waitForUnstableElement(By.xpath("//label[@for='important-information']"));
    }

    public WebElement submitBtn() {
        return waitForUnstableElement(By.id("submit-button"));
    }

}
