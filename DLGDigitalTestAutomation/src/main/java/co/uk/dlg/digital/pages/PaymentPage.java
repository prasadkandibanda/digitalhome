package co.uk.dlg.digital.pages;

import com.usmanhussain.habanero.framework.AbstractPage;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

public class PaymentPage extends AbstractPage {

    public WebElement monthlyPaymentSelect() {
        return waitForElementPresent(By.id("monthlySelect"));
    }

    public WebElement annualPaymentSelect() {
        return waitForElementPresent(By.id("annualSelect"));
    }

    public WebElement cardYourNameRadioButonYes() {
        return waitForElementPresent(By.xpath("//*[@data-test-id='card-policy-true']"));
    }

    public WebElement cardYourNameRadioButonNo() {
        return waitForElementPresent(By.xpath("//*[@data-test-id='card-policy-false']"));
    }

    public WebElement cardHolderNameField() {
        return waitForElementPresent(By.id("cardHolder"));
    }

    public WebElement cardNumberField() {
        return waitForElementPresent(By.id("cardNumber"));
    }

    public WebElement cardSecurityNumberField() {
        return waitForElementPresent(By.id("securityNumber"));
    }

    public WebElement issueNumberField() {
        return waitForElementPresent(By.id("issueNumber"));
    }

    public WebElement cardExpiryMonthDropDownField() {
        return waitForElementPresent(By.id("expiryDateMM"));
    }

    public WebElement cardExpiryYearDropDownField() {
        return waitForElementPresent(By.id("expiryDateYY"));
    }

    public WebElement startDateMMDropDown() {
        return waitForElementPresent(By.id("startDateMM"));
    }

    public WebElement startDateYYDropDown() {
        return waitForElementPresent(By.id("startDateYY"));
    }

    public WebElement ukBillingAddressYesRadioButton() {
        return waitForElementPresent(By.xpath("//*[@data-test-id='card-has-uk-true']"));
    }

    public WebElement ukBillingAddressNoRadioButton() {
        return waitForElementPresent(By.xpath("//*[@data-test-id='card-has-uk-false']"));
    }

    public WebElement billingLine1Field() {
        return waitForElementPresent(By.id("billingLine1"));
    }

    public WebElement billingLine2Field() {
        return waitForElementPresent(By.id("billingLine2"));
    }

    public WebElement countryField() {
        return waitForElementPresent(By.id("country"));
    }

    public WebElement saveForLaterBtn() {
        return waitForElementPresent(By.id("save-button"));
    }

    public WebElement backBtn() {
        return waitForElementPresent(By.id("back-button"));
    }

    public WebElement buyNowButton() {
        return waitForUnstableElement(By.id("buy-now-submit"));
    }

    // Payment Summary Light Window Elements:-------------------------------------------------------------------

    // for Monthly DirectDebit  Final Buy button
    public WebElement finalBuyNowButtonDD() {
        return waitAndFindElement(By.id("dd-buy"));
    }

    // for Annual payemnt finla buy button
    public WebElement finalBuyNowButtonCC() {
        return waitAndFindElement(By.id("cc-buy"));
    }

    public WebElement editPaymentDetailsButton() {
        return waitAndFindElement(By.xpath("//*[@data-test-id='final-card-edit']"));
    }


    // MONTHLY SELECT,  DIRECT DEBIT PAYMENT METHOD ELEMENTS.


    public WebElement policyHolderDDRadioButtonYes() {
        return waitForElementPresent(By.xpath("//*[@for='policyholderDD-yes']"));
    }

    public WebElement accoutHolderNameField() {
        return waitForElementPresent(By.id("accountName"));
    }

    public WebElement buildingSocietyNameField() {
        return waitForElementPresent(By.id("buildingSocietyName"));
    }

    public WebElement sortCode1Field() {
        return waitForElementPresent(By.id("sortCode1"));
    }

    public WebElement sortCode2Field() {
        return waitForElementPresent(By.id("sortCode2"));
    }

    public WebElement sortCode3Field() {
        return waitForElementPresent(By.id("sortCode3"));
    }

    public WebElement accountNumberField() {
        return waitForElementPresent(By.id("accountNumber"));
    }

    public WebElement secciCheckBox() {
        return waitForElementPresent(By.xpath("//*[@for='secci-confirmation']"));
    }


}
