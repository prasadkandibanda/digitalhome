package co.uk.dlg.digital.pages;

import com.usmanhussain.habanero.framework.AbstractPage;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

public class ReviewAndConfirmPage extends AbstractPage {

    public WebElement editYourCover() {
        return waitForElementPresent(By.linkText("Edit your cover"));
    }

    public WebElement editYourExtras() {
        return waitForElementPresent(By.linkText("Edit your extras"));
    }

    public WebElement editYourDetails() {
        return waitForElementPresent(By.id("personal-details"));
    }

    public WebElement policyHolderName() {
        return waitForElementPresent(By.id("policy-holder"));
    }

    public WebElement editYourHome() {
        return waitForElementPresent(By.id("home-details"));
    }

    public WebElement changeMailingAddress() {
        return waitForElementPresent(By.id("corr-address"));
    }

    public WebElement addMortgageLenderDetails() {
        return waitForElementPresent(By.id("mortgage-lender-details"));
    }

    public WebElement selectBackButton() {
        return waitForElementPresent(By.id("back-button"));
    }

    public WebElement selectSaveButton() {
        return waitForElementPresent(By.id("save-button"));
    }

    public WebElement selectPrintButton() {
        return waitForElementPresent(By.id("print-button"));
    }

    public WebElement selectNoIDisagreeButton() {
        return waitForElementPresent(By.id("no-i-disagree"));
    }


    public WebElement selectYesIAgreeButton() {
        return waitForElementPresent(By.id("yes-i-agree-button"));
    }

}
