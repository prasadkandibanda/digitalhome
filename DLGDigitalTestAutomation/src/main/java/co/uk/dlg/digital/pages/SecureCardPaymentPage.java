package co.uk.dlg.digital.pages;

import com.usmanhussain.habanero.framework.AbstractPage;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

public class SecureCardPaymentPage extends AbstractPage {

    public WebElement securePassword() {
        return waitForElementPresent(By.name("Password"));
    }

    public WebElement secureSubmit() {
        return waitForElementPresent(By.name("a1"));
    }

    public WebElement frameElement() {
        return waitForElementPresent(By.id("dataifame"));
    }

}
