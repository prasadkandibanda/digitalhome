package co.uk.dlg.digital.utils;

import co.uk.dlg.digital.pages.*;
import com.usmanhussain.habanero.framework.AbstractPage;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;

import java.util.List;

public class HomeUtil extends AbstractPage {

    private YourDetails yourDetails = new YourDetails();
    private PremiumPage premiumPage = new PremiumPage();
    private ReviewAndConfirmPage reviewAndConfirmPage = new ReviewAndConfirmPage();
    private PaymentPage paymentPage = new PaymentPage();
    private SecureCardPaymentPage secureCardPaymentPage = new SecureCardPaymentPage();
    private ConfirmationPage confirmationPage = new ConfirmationPage();
    private QpmSaveLoginDetailsPage qpmSaveLoginDetailsPage = new QpmSaveLoginDetailsPage();
    private PortfolioPage portfolioPage = new PortfolioPage();

    public void fillPostcode(String postCode) throws InterruptedException {
        yourDetails.postCode().sendKeys(postCode);
        scrollToViewClick(yourDetails.findMyAddr());
        yourDetails.chooseAddr().click();
    }

    public void fillHomeDetails(String homeDetails1, String homeDetails2, String RoomsPeople, String CoverStart, String FillClaims) throws InterruptedException {
        String OwnRentType = homeDetails1.split(",")[0];
        String PropertyType = homeDetails1.split(",")[3];
        scrollToViewClick(yourDetails.ownerShip(homeDetails1.split(",")[0]));
        if (OwnRentType.equalsIgnoreCase("Mortgaged") || OwnRentType.equalsIgnoreCase("Owned")) {
            scrollToViewClick(yourDetails.coverType(homeDetails1.split(",")[1]));
        } else {
            ((JavascriptExecutor) getDriver).executeScript("arguments[0].click();", yourDetails.coverType("Contents only"));
        }
        yourDetails.approxYrBuilt().sendKeys(homeDetails1.split(",")[2]);
        if (PropertyType.equalsIgnoreCase("Maisonette")) {
            scrollToViewClick(yourDetails.propertyType(homeDetails1.split(",")[3]));
        } else {
            yourDetails.propertyType(homeDetails1.split(",")[3]).click();
            scrollToViewClick(yourDetails.describeProperty(homeDetails2.split(",")[0]));
        }
        yourDetails.floodedNo().click();
        scrollToViewClick(yourDetails.extWallType(homeDetails2.split(",")[1]));
        scrollToViewClick(yourDetails.roofType(homeDetails2.split(",")[2]));
        yourDetails.bedRooms(RoomsPeople.split(",")[0]).click();
        yourDetails.bathRooms(RoomsPeople.split(",")[1]).click();
        scrollToViewClick(yourDetails.adults(RoomsPeople.split(",")[2]));
        yourDetails.children(RoomsPeople.split(",")[3]).click();
        new Select(yourDetails.coverStartMonthDropdown()).selectByVisibleText(CoverStart.split(",")[0]);
        new Select(yourDetails.coverStartDayDropdown()).selectByVisibleText(CoverStart.split(",")[1]);

        scrollToViewClick(yourDetails.nextBtn());
        String NumberOfClaims = FillClaims.split(",")[0];
        int claims = Integer.parseInt(NumberOfClaims);
        if (claims == 0) {
            scrollToViewClick(yourDetails.noOfClaims(FillClaims.split(",")[0]));
        } else {
            scrollToViewClick(yourDetails.noOfClaims(FillClaims.split(",")[0]));
            waitForMoreTime();
            for (int i = 1; i <= claims; i++) {
                new Select(yourDetails.claimMonthSelect(i)).selectByVisibleText("Mar");
                new Select(yourDetails.claimYearSelect(i)).selectByVisibleText("2016");
                new Select(yourDetails.typeOfClaimSelect(i)).selectByVisibleText("Fire");
                yourDetails.claimValueEnter(i).sendKeys("5000");
                scrollToViewClick(yourDetails.claimAddressYesButton(i));
            }
        }
        yourDetails.claimFreeYearsBuildings(FillClaims.split(",")[1]).click();
        yourDetails.claimFreeYearsContents(FillClaims.split(",")[2]).click();
        yourDetails.nextBtn().click();
    }

    public void fillAboutYourSelf(String customerName, String customerDetails, String paymentType) throws InterruptedException {
        yourDetails.title(customerDetails.split(",")[0]).click();
        yourDetails.firstName().sendKeys(customerName.split(" ")[0]);
        yourDetails.lastName().sendKeys(customerName.split(" ")[1]);
        yourDetails.dateDOB().sendKeys(customerDetails.split(",")[1].split("/")[0]);
        yourDetails.monthDOB().sendKeys(customerDetails.split(",")[1].split("/")[1]);
        yourDetails.yearDOB().sendKeys(customerDetails.split(",")[1].split("/")[2]);
        yourDetails.email().sendKeys(customerDetails.split(",")[2]);
        ((JavascriptExecutor) getDriver).executeScript("arguments[0].click();", yourDetails.policyPaymentMethodAnnually());

        //Clickon Next Button After entering about your self detials
        scrollToViewClick(yourDetails.nextBtn());

        //Clickon Next Button  After Additional Details Entered
        scrollToViewClick(yourDetails.nextBtn());

        // Click on Next button After Privacy Statement.
        scrollToViewClick(yourDetails.nextBtn());

        // Enter important Impormation and Submit YOur Details Form
        // in privilege page check box styling is different so we need to uee java script to click on checkbox.

        ((JavascriptExecutor) getDriver).executeScript("arguments[0].click();", yourDetails.impInfoChkBox());
        yourDetails.submitBtn().click();
    }

    public void enterPremiumDetails(String coverProduct, String paymentType) {
        if (coverProduct.equalsIgnoreCase("HomePlus")) {
            ((JavascriptExecutor) getDriver).executeScript("arguments[0].click();", premiumPage.selectHomeInsurancePlus());
        } else {
            ((JavascriptExecutor) getDriver).executeScript("arguments[0].click();", premiumPage.personalItemsOutsideHomeNo());
            ((JavascriptExecutor) getDriver).executeScript("arguments[0].click();", premiumPage.accidentalDamageNo());
            ((JavascriptExecutor) getDriver).executeScript("arguments[0].click();", premiumPage.homeEmergencyNo());
            premiumPage.familyLegalNo().click();
            ((JavascriptExecutor) getDriver).executeScript("arguments[0].click();", premiumPage.selectHomeInsurance());
        }
        if (paymentType.equalsIgnoreCase("Annually")) {
            ((JavascriptExecutor) getDriver).executeScript("arguments[0].click();", premiumPage.homeSingleAnnualPayment());
        } else {
            ((JavascriptExecutor) getDriver).executeScript("arguments[0].click();", premiumPage.homeMonthlyPayment());
        }
    }

    public void reviewAndConfirm(String customerName) throws InterruptedException {
        scrollToViewClick(premiumPage.reviewAndConfirmBtn());
        waitForPageLoad();
        // Reviewing the policy holider name and continue to YesAgree Butoon
        Assert.assertTrue(reviewAndConfirmPage.policyHolderName().getText().equalsIgnoreCase(customerName));
        scrollToViewClick(reviewAndConfirmPage.selectYesIAgreeButton());
    }

    public void fillpaymentPage(String customerName, String paymentType, String paymentDetails) throws InterruptedException {
        if (paymentType.equals("Annually")) {
            scrollToViewClick(paymentPage.cardYourNameRadioButonYes());
            paymentPage.cardHolderNameField().sendKeys("jackDeniel");
            paymentPage.cardNumberField().sendKeys(paymentDetails.split(",")[0]);
            paymentPage.cardSecurityNumberField().sendKeys(paymentDetails.split(",")[1]);
            new Select(paymentPage.cardExpiryMonthDropDownField()).selectByVisibleText("05");
            new Select(paymentPage.cardExpiryYearDropDownField()).selectByVisibleText("2020");
            //  paymentPage.ukBillingAddressYesRadioButton().click();
            scrollToViewClick(paymentPage.buyNowButton());

            //Light Window Finaly Buy Button
            waitForMoreTime();
            scrollToViewClick(paymentPage.finalBuyNowButtonCC());
        } else {
            scrollToViewClick(paymentPage.policyHolderDDRadioButtonYes());
            paymentPage.accoutHolderNameField().sendKeys(customerName);
            paymentPage.buildingSocietyNameField().sendKeys("halifax");
            paymentPage.sortCode1Field().sendKeys("11");
            paymentPage.sortCode2Field().sendKeys("01");
            paymentPage.sortCode3Field().sendKeys("01");
            paymentPage.accountNumberField().sendKeys("11111110");
            scrollToViewClick(paymentPage.secciCheckBox());
            scrollToViewClick(paymentPage.buyNowButton());
            //Light Window Finaly Buy Button
            waitForMoreTime();
            paymentPage.finalBuyNowButtonDD().click();
        }
    }

    public void fillSecuryPaymentPage() {
        paymentPage.waitForPageLoad();
        paymentPage.waitForMoreTime();
        getDriver.switchTo().frame(paymentPage.waitForElementPresent(By.id("dataifame")));
        secureCardPaymentPage.securePassword().sendKeys("12345");
        scrollToViewClick(secureCardPaymentPage.secureSubmit());
        getDriver.switchTo().defaultContent();
    }

    public void verifyConfirmationPage() throws InterruptedException {
        if (getDriver.getCurrentUrl().contains("directline")) {
            Assert.assertTrue(confirmationPage.dlconfirmationText().isDisplayed());
        } else if (getDriver.getCurrentUrl().contains("churchill") || (getDriver.getCurrentUrl().contains("privilege"))) {
            Assert.assertTrue(confirmationPage.confirmationImg().isDisplayed());
        }
    }

    public void saveQuoteAndSubmitRegistration(String customerDetails, String paymentType) throws InterruptedException {
        scrollToViewClick(premiumPage.saveForLaterBtn());
        if (getDriver.getCurrentUrl().contains("login")) {
            qpmSaveLoginDetailsPage.firstNthCharField().sendKeys("1");
            qpmSaveLoginDetailsPage.secondNthCharField().sendKeys("1");
            qpmSaveLoginDetailsPage.thirdNthCharField().sendKeys("1");
            qpmSaveLoginDetailsPage.fourthNthCharField().sendKeys("1");
            qpmSaveLoginDetailsPage.loginButton().click();
            waitForPageLoad();
            qpmSaveLoginDetailsPage.saveAndFinishButton().click();
        } else {
            qpmSaveLoginDetailsPage.emailAddressField().sendKeys(customerDetails.split(",")[2]);
            qpmSaveLoginDetailsPage.confirmEmailAddressField().sendKeys(customerDetails.split(",")[2]);
            qpmSaveLoginDetailsPage.passwordField().sendKeys("11111111");
            qpmSaveLoginDetailsPage.confirmPasswordField().sendKeys("11111111");
            new Select(qpmSaveLoginDetailsPage.selectSecurityQuestionDropdown()).selectByValue("1");
            qpmSaveLoginDetailsPage.answerToSecurityQuestionField().sendKeys("11111111");
            qpmSaveLoginDetailsPage.confirmSecurityQuestionField().sendKeys("11111111");
            scrollToViewClick(qpmSaveLoginDetailsPage.submitButton());
            waitForMoreTime();
            qpmSaveLoginDetailsPage.saveAndFinishButton().click();
        }
        waitForMoreTime();
        scrollToViewClick(portfolioPage.portfolioLink());
        waitForMoreTime();
        scrollToViewClick(portfolioPage.reviewAndBuyButton());
        waitForMoreTime();
        //premiumPage.homeSingleAnnualPayment().click();
        if (paymentType.equalsIgnoreCase("Annually")) {
            ((JavascriptExecutor) getDriver).executeScript("arguments[0].click();", premiumPage.homeSingleAnnualPayment());
        } else {
            ((JavascriptExecutor) getDriver).executeScript("arguments[0].click();", premiumPage.homeMonthlyPayment());
            // premiumPage.homeMonthlyPayment().click();
        }
    }

    public void scrollToViewClick(WebElement element) {
        ((JavascriptExecutor) getDriver).executeScript("arguments[0].scrollIntoView(true);", element);
        waitForMoreTime();
        element.click();
    }

    public void validatePolicyMiniumExcessValue() {

        waitForPageLoad();

 // checking HOM-1661 , minimum policy excess value start from £100, previously £50 for DL.

        // Standard Policy - Home policy excess dropdown Selection validation
        List<WebElement> allPolicyExcessValues = new Select(premiumPage.standardPolicyExcessSelect()).getOptions();
        String HomeMiniumExcessValue = allPolicyExcessValues.iterator().next().getText();

        Assert.assertTrue("The minimum excess value for Home is £100", HomeMiniumExcessValue.equalsIgnoreCase("£100"));

        //Home plus policy excess dropdown selection validation.

        List<WebElement> options = new Select(premiumPage.homePlusPolicyExcessSelect()).getOptions();
        String HomePlusMinimumExcessValue = options.iterator().next().getText();
        Assert.assertTrue("The minimum excess value for HomePlus is : £100", HomePlusMinimumExcessValue.equalsIgnoreCase("£100"));

// Checkin HOM-1774 , Default policy excess value should be £250, before it was £200 for all brands.

        String DefaultValue =  new Select(premiumPage.standardPolicyExcessSelect()).getFirstSelectedOption().getText();
        System.out.println("The default excess value is :  "+DefaultValue);
        Assert.assertTrue("The default excess value for Home is : £250", DefaultValue.equalsIgnoreCase("£250"));

        String DefaultValueHomePlus =  new Select(premiumPage.homePlusPolicyExcessSelect()).getFirstSelectedOption().getText();
        System.out.println("The default excess value is :  "+DefaultValueHomePlus);
        Assert.assertTrue("The default excess value for HomePlus is : £250", DefaultValueHomePlus.equalsIgnoreCase("£250"));
    }


    public void validatePolicyDefaultExcessValue() {

// Checkin HOM-1774 , Default policy excess value should be £250, before it was £200 for all brands.

        String DefaultValue =  new Select(premiumPage.standardPolicyExcessSelect()).getFirstSelectedOption().getText();
        System.out.println("The default excess value is :  "+DefaultValue);
        Assert.assertTrue("The default excess value for Home is : £250", DefaultValue.equalsIgnoreCase("£250"));

        String DefaultValueHomePlus =  new Select(premiumPage.homePlusPolicyExcessSelect()).getFirstSelectedOption().getText();
        System.out.println("The default excess value is :  "+DefaultValueHomePlus);
        Assert.assertTrue("The default excess value for HomePlus is : £250", DefaultValueHomePlus.equalsIgnoreCase("£250"));

    }


    public void validateContentsSumInsured() {

        waitForPageLoad();
        waitForMoreTime();

       ((JavascriptExecutor) getDriver).executeScript("arguments[0].scrollIntoView(true);", premiumPage.contentsSumInsuredInput());
       // premiumPage.contentsSumInsuredInput().click();
       // premiumPage.contentsSumInsuredInput().clear();
        premiumPage.contentsSumInsuredInput().sendKeys("75000");

    }
}
