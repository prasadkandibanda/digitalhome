package co.uk.dlg.digital.utils.common;

import com.usmanhussain.habanero.framework.AbstractPage;
import org.openqa.selenium.*;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.List;

public class CommonUtil extends AbstractPage {

    public void setValue(List<List<String>> data, String fieldName, WebElement property) throws Throwable {
        String strValue1 = datapicker(data, fieldName);
        if (!strValue1.equalsIgnoreCase("")) {
            property.sendKeys(strValue1);
        } else {
            System.out.println("input Field is not mandatory or Null");
        }
    }

    public void clickElement(WebElement property) throws Throwable {
        property.click();
    }

    public String datapicker(List<List<String>> data, String fieldName) {
        boolean flag = false;
        String strValue = null;
        for (int i = 0; i < data.size(); i++) {
            for (int k = 0; k < data.get(0).size(); k++) {
                String searchKey = data.get(0).get(k).toString();
                if (searchKey.equalsIgnoreCase(fieldName)) {
                    strValue = data.get(1).get(k).toString();
                    flag = true;
                    break;
                }
                if (flag == true) {
                    break;
                }
            }
        }
        return strValue;
    }

    public void switchFrame(String frameName) throws Throwable {
        getDriver.switchTo().frame(frameName);
    }

    public void selectElement(List<List<String>> data, String fieldName, WebElement property) throws Throwable {
        String title = datapicker(data, fieldName);
        if (!title.equalsIgnoreCase("")) {
            new Select(property).selectByVisibleText(title);
        } else {
            System.out.println("Input data is not mandatory to set  ");
        }
    }

    public void selectElementByJS(List<List<String>> data, String fieldName, String property) throws Throwable {
        JavascriptExecutor executor = (JavascriptExecutor) getDriver;
        String dbData = datapicker(data, fieldName);
        String actElement = property.replace("propData", dbData);
        if (!dbData.equalsIgnoreCase("")) {
            WebElement clickElement = getDriver.findElement(By.xpath(actElement));
            executor.executeScript("arguments[0].click();", clickElement);
        } else {
            System.out.println("Input data is not mandatory to set  ");
        }
    }

    public void clickbyJS(WebElement strProperty) {
        JavascriptExecutor executor = (JavascriptExecutor) getDriver;
        executor.executeScript("arguments[0].click();", strProperty);
    }

    public void tabKeypress() throws Exception {
        Actions action = new Actions(getDriver);
        action.sendKeys(Keys.TAB).build().perform();
    }

    public void setValue(String data, WebElement property) throws Throwable {
        if (!data.equalsIgnoreCase("")) {
            property.sendKeys(data);
        } else {
            System.out.println("input Field is not mandatory or Null");
        }
    }

    public void staticwaitbyXpath() {
        boolean jQcondition = false;
        try {
            new WebDriverWait(getDriver, 20) {
            }.until(new ExpectedCondition<Boolean>() {
                @Override
                public Boolean apply(WebDriver driverObject) {
                    try {
                        Thread.sleep(1000);
                    } catch (InterruptedException e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                    }
                    return (Boolean) ((JavascriptExecutor) driverObject).executeScript("return jQuery.active == 0");
                }
            });
            jQcondition = (Boolean) ((JavascriptExecutor) getDriver).executeScript("return window.jQuery != undefined && jQuery.active === 0");
            if (jQcondition) return;
            Thread.sleep(1000);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

}