package co.uk.dlg.digital.pages;

import com.usmanhussain.habanero.framework.AbstractPage;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

public class QpmSaveLoginDetailsPage extends AbstractPage {

    public WebElement emailAddressField() {
        return waitForElementPresent(By.id("emailAddress"));
    }

    public WebElement confirmEmailAddressField() {
        return waitForElementPresent(By.id("emailConfirm"));
    }

    public WebElement passwordField() {
        return waitForElementPresent(By.id("password"));
    }

    public WebElement confirmPasswordField() {
        return waitForElementPresent(By.id("passwordConfirm"));
    }

    public WebElement selectSecurityQuestionDropdown() {
        return waitForElementPresent(By.id("securityQuestion"));
    }

    public WebElement answerToSecurityQuestionField() {
        return waitForElementPresent(By.id("securityAnswer"));
    }

    public WebElement confirmSecurityQuestionField() {
        return waitForElementPresent(By.id("securityAnswerConfirm"));
    }

    // If Registered User it will go for login details page the element for this are:
    public WebElement firstNthCharField() {
        return waitForElementPresent(By.id("firstNthChar"));
    }

    public WebElement secondNthCharField() {
        return waitForElementPresent(By.id("secondNthChar"));
    }

    public WebElement thirdNthCharField() {
        return waitForElementPresent(By.id("thirdNthChar"));
    }

    public WebElement fourthNthCharField() {
        return waitForElementPresent(By.id("fourthNthChar"));
    }

    public WebElement loginButton() {
        return waitForElementPresent(By.id("login-button"));
    }

    public WebElement carinsuranceLink() {
        return waitAndFindElement(By.xpath("//li[@class='link-car']/a"));
    }

    public WebElement homeinsuranceLink() {
        return waitAndFindElement(By.xpath("//li[@class='link-home']/a"));
    }

    public WebElement petinsuranceLink() {
        return waitAndFindElement(By.xpath("//li[@class='link-pet']/a"));
    }

    public WebElement travelinsuranceLink() {
        return waitAndFindElement(By.xpath("//li[@class='link-travel']/a"));
    }

    public WebElement breakdownCoverLink() {
        return waitAndFindElement(By.xpath("//li[@class='link-breakdown']/a"));
    }

    public WebElement vaninsuranceLink() {
        return waitAndFindElement(By.xpath("//li[@class='link-van']/a"));
    }

    public WebElement quoteTransferButton() {
        return waitAndFindElement(By.xpath("//*[@data-reveal-id='transfer-modal']"));
    }

    public WebElement submitButton() {
        return waitForElementPresent(By.xpath("//*[@data-test-id='registration-submit-btn']"));
    }

    public WebElement saveAndFinishButton() {
        return waitAndFindElement(By.xpath("//*[@data-test-id='save-and-finish']"));
    }

    public WebElement getAMotorQuote() {
        return waitForElementPresent(By.xpath("//*[@id='link-car-insurance']"));
    }

}
