package co.uk.dlg.digital.pages;

import com.usmanhussain.habanero.framework.AbstractPage;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

public class PremiumPage extends AbstractPage {

    // ------------------------Your cover limits Web Elements  ---------------------------------------------
    public WebElement yourCoverLevelDropdown() {
        return waitForElementPresent(By.xpath("//*[@id='select-standard-cover-type']"));
    }

    public WebElement contentsSumInsuredInput() {  return waitForElementPresent(By.id("contentsSumInsuredInput"));    }

    public WebElement contentsSumInsuredPlusInput() { return  waitForElementPresent(By.id("contentsSumInsuredPlusInput")) ; }

    public WebElement contentsSumSave() {
        return waitForElementPresent(By.xpath("//*[@data-test-id='cont-sum-save-btn']"));
    }

    // ----------------------Your personal items Web Elements  -----------------------------------------------------------------------
    public WebElement personalItemsOutsideHomeYes() {
        return waitForElementPresent(By.xpath("//*[@data-test-id='un-pers-poss-true']"));
    }

    public WebElement personalItemsOutsideHomeNo() {
        return waitForUnstableElement(By.xpath("//*[@data-test-id='un-pers-poss-false']"));
    }

    public WebElement personalItemsOutsideInput() {
        return waitForElementPresent(By.xpath("//*[@id='personalItemsOutsideInput']"));
    }

    public WebElement itemsOutsideSaveBtn() {
        return waitForElementPresent(By.xpath("//*[@id='itemsOutsideSaveBtn']"));
    }

    public WebElement homePlusPersonalItemsOutsideInput() {
        return waitForElementPresent(By.xpath("//*[@id='homePlusPersonalItemsOutsideInput']"));
    }

    public WebElement homePlusItemsOutsideSaveBtn() {
        return waitForElementPresent(By.xpath("//*[@id='homePlusItemsOutsideSaveBtn']"));
    }

    public WebElement addHighValueItemsCoverStandard() {
        return waitForElementPresent(By.linkText("Add items over £2,000"));
    }

    public WebElement addHighValueItemsCoverHomePlus() {
        return waitForElementPresent(By.linkText(" Add items over £4,000"));
    }

    public WebElement addBicyclesCoverStandard() {
        return waitForElementPresent(By.linkText("Add bicycles over £500"));
    }

    public WebElement addBicyclesCoverHomePlus() {
        return waitForElementPresent(By.linkText("Add bicycles over £1,000"));
    }

    // ------------------------------   YOUR EXCESSES SECTION WEB ELEMENTS ------------------------------------
    public WebElement standardPolicyExcessSelect() {
        return waitForElementPresent(By.id("standard-excess-select"));
    }

    public WebElement homePlusPolicyExcessSelect() {
        return waitForElementPresent(By.id("plus-excess-select"));
    }

    // -------------------------------------------YOUR OPTIONAL EXTRAS SECTION WEB ELEMENTS----------------------------
    public WebElement accidentalDamageYes() {
        return waitForElementPresent(By.xpath("//*[@data-test-id='acc-id-true']"));
    }

    public WebElement accidentalDamageNo() {
        return waitForElementPresent(By.xpath("//*[@data-test-id='acc-id-false']"));
    }

    public WebElement homeEmergencyYes() {
        return waitForElementPresent(By.xpath("//*[@data-test-id='home-emergency-true']"));
    }

    public WebElement homeEmergencyNo() {
        return waitForUnstableElement(By.xpath("//*[@data-test-id='home-emergency-false']"));
    }

    public WebElement familyLegalYes() {
        return waitForElementPresent(By.xpath("//*[@data-test-id='fam-leg-prot-true']"));
    }

    public WebElement familyLegalNo() {
        return waitForElementPresent(By.xpath("//*[@data-test-id='fam-leg-prot-false']"));
    }

    public WebElement selectHomeInsurance() {
        return waitForElementPresent(By.id("selectHomeInsurance"));
    }

    public WebElement selectHomeInsurancePlus() {
        return waitForElementPresent(By.id("selectHomeInsurancePlus"));
    }

    public WebElement homeSingleAnnualPayment() {
        return waitForUnstableElement(By.id("homeSinglePayment"));
    }

    public WebElement homeMonthlyPayment() {
        return waitForUnstableElement(By.id("homeMonthlyPayment"));
    }

    public WebElement saveForLaterBtn() {
        return waitForElementPresent(By.id("save-button"));
    }

    public WebElement reviewAndConfirmBtn() {
        return waitForElementPresent(By.xpath("//*[@id='next-button']"));
    }

    public WebElement backButton() {
        return waitForElementPresent(By.id("back-button"));
    }

}
