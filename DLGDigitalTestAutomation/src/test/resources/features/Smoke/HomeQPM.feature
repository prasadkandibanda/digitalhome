Feature: Home Inusance Quote Policy Manager-QPM Journey
@smoke1
Scenario Outline: Purchage Quote Policy Manager(QPM) Successfully '<brandName>'
  Given I launch home application '<brandName>'
  When enter the '<postCode>','<homeDetails1>','<homeDetails2>','<roomsAndPeople>','<coverStart>','<claims>','<customerName>','<customerDetails>','<paymentType>'
  Then check '<homeDetails1>','<paymentType>' and enter the Premium Details
  And clicked on save quote button and fill login Registraion form '<customerDetails>','<paymentType>' and submit
  And review '<customerName>' details and purchase policy using option '<paymentType>' and '<paymentDetails>'
  Then I should get the confirmation for my policy purchase


Examples:
  |brandName  | postCode  | homeDetails1                               | homeDetails2              | roomsAndPeople | coverStart  | claims  |customerName|customerDetails                 |paymentType|paymentDetails      |
  #|directline |  RM81RR   | Mortgaged,Building and Contents,1977,House | Detached House,Brick,None | 2,2,3,1        |   Nov,13th   | 0,0,0  | prasad sname|Mr,01/02/1964,test1509@hip.com |Annually   |4444333322221111,123|
  #|privilege  |  RM81RR   | Mortgaged,Building and Contents,1977,House | Detached House,Brick,None | 2,2,3,1        |   Nov,13th   | 0,0,0  | prasad sname|Mr,01/02/1964,test1509@hip.com |Annually   |4444333322221111,123|
  #|churchill  |  RM81RR   | Mortgaged,Building and Contents,1977,House | Detached House,Brick,None | 2,2,3,1        |   Nov,13th   | 0,0,0  | prasad sname|Mr,01/02/1964,test1509@hip.com |Annually   |4444333322221111,123|
  |directline |  RM81RR   | Mortgaged,Building and Contents,1977,House | Detached House,Brick,None | 2,2,3,1        |   Nov,13th   | 0,0,0  | prasad sname|Mr,01/02/1964,test1509@hip.com |Monthly    |4444333322221111,123|
  |privilege  |  RM81RR   | Mortgaged,Building and Contents,1977,House | Detached House,Brick,None | 2,2,3,1        |   Nov,13th   | 0,0,0  | prasad sname|Mr,01/02/1964,test1509@hip.com |Monthly    |4444333322221111,123|
  |churchill  |  RM81RR   | Mortgaged,Building and Contents,1977,House | Detached House,Brick,None | 2,2,3,1        |   Nov,13th   | 0,0,0  | prasad sname|Mr,01/02/1964,test1509@hip.com |Monthly    |4444333322221111,123|


