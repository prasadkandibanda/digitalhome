#Sprint 20 automatable tickets
  #HOM-1661 - Direct Line Compulsory excess should be £100.
             # Hence, drop down text on quote and buy site for overall policy excess should have a minimum excess value of £100.
  #HOM-1774 - System default option for policy excess will be £250 for own brands (Direct Line, Churchill and Privilege)
  #HOM-1766 - Has the contents sum insured amount been amended to allow up to £105,000?

@sprint20
  Feature: Validate Minimum amd default excess values.

    Scenario Outline: Check Confirmation of policy excess should have minimum value of £100 for DL
      Given I launch home application '<brandName>'
      When enter the '<postCode>','<homeDetails1>','<homeDetails2>','<roomsAndPeople>','<coverStart>','<claims>','<customerName>','<customerDetails>','<paymentType>'
      Then Validate policy Excess Values and Confirm
      #Then check '<homeDetails1>','<paymentType>' and enter the Premium Details
      #And review '<customerName>' details and purchase policy using option '<paymentType>' and '<paymentDetails>'
      #Then I should get the confirmation for my policy purchase

      Examples:
        |brandName  | postCode   | homeDetails1                               | homeDetails2              | roomsAndPeople  | coverStart  | claims  |customerName |customerDetails            |paymentType |paymentDetails      |
        #|directline |  RM81RR    | Mortgaged,Building and Contents,1975,House | Detached House,Brick,None | 2,2,3,1         |   Nov,13th   | 0,0,0  | prasad sname|Mr,01/01/1969,test@hip.com |Monthly     |4444333322221111,123|
        |churchill  |  RM81RR    | Mortgaged,Building and Contents,1975,House | Detached House,Brick,None | 2,2,3,1         |   Nov,13th   | 0,0,0  | prasad sname|Mr,01/01/1969,test@hip.com |Monthly     |4444333322221111,123|
        #|privilege  |  RM81RR    | Mortgaged,Building and Contents,1975,House | Detached House,Brick,None | 2,2,3,1         |   Nov,13th   | 0,0,0  | prasad sname|Mr,01/01/1969,test@hip.com |Monthly     |4444333322221111,123|

