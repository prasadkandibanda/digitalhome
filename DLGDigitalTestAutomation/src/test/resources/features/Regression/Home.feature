
Feature: Home Inusance Quote & Buy Journey For all Brands - Payment Successfully

  @Reg
  Scenario Outline: Purchage Quote and Buy Successfully '<brandName>' cover product Home-Annually & Monthly
    Given I launch home application '<brandName>'
    When enter the '<postCode>','<homeDetails1>','<homeDetails2>','<roomsAndPeople>','<coverStart>','<claims>','<customerName>','<customerDetails>','<paymentType>'
    Then check '<coverProduct>','<paymentType>' and enter the Premium Details
    And review '<customerName>' details and purchase policy using option '<paymentType>' and '<paymentDetails>'
    Then I should get the confirmation for my policy purchase

    Examples:
      |brandName  | postCode   | homeDetails1                               | homeDetails2              | roomsAndPeople  | coverStart  | claims  |customerName |customerDetails           |coverProduct |paymentType  |paymentDetails      |
      |directline |  RM81RR    | Mortgaged,Building and Contents,1975,House | Detached House,Brick,None | 2,2,3,1         |   Nov,13th   | 1,0,0  | prasad sname|Mr,01/01/1969,test@hip.com| Home        |Annually     |4444333322221111,123|
      |privilege  |  RM81RR    | Mortgaged,Building and Contents,1975,House | Detached House,Brick,None | 2,2,3,1         |   Nov,13th   | 0,0,0  | prasad sname|Mr,01/01/1969,test@hip.com| Home        |Annually     |4444333322221111,123|
      |churchill  |  RM81RR    | Mortgaged,Building and Contents,1975,House | Detached House,Brick,None | 2,2,3,1         |   Nov,13th   | 0,0,0  | prasad sname|Mr,01/01/1969,test@hip.com| Home        |Annually     |4444333322221111,123|
      |directline |  RM81RR    | Mortgaged,Building and Contents,1975,House | Detached House,Brick,None | 2,2,3,1         |   Nov,13th   | 1,0,0  | prasad sname|Mr,01/01/1969,test@hip.com| Home        |Monthly      |4444333322221111,123|
      |privilege  |  RM81RR    | Mortgaged,Building and Contents,1975,House | Detached House,Brick,None | 2,2,3,1         |   Nov,13th   | 0,0,0  | prasad sname|Mr,01/01/1969,test@hip.com| Home        |Monthly      |4444333322221111,123|
      |churchill  |  RM81RR    | Mortgaged,Building and Contents,1975,House | Detached House,Brick,None | 2,2,3,1         |   Nov,13th   | 0,0,0  | prasad sname|Mr,01/01/1969,test@hip.com| Home        |Monthly      |4444333322221111,123|


  @Reg
  Scenario Outline: Purchage Quote and Buy Successfully '<brandName>' cover product HomePlus-Annually & Monthly
    Given I launch home application '<brandName>'
    When enter the '<postCode>','<homeDetails1>','<homeDetails2>','<roomsAndPeople>','<coverStart>','<claims>','<customerName>','<customerDetails>','<paymentType>'
    Then check '<coverProduct>','<paymentType>' and enter the Premium Details
    And review '<customerName>' details and purchase policy using option '<paymentType>' and '<paymentDetails>'
    Then I should get the confirmation for my policy purchase

    Examples:
      |brandName  | postCode   | homeDetails1                               | homeDetails2              | roomsAndPeople  | coverStart  | claims  |customerName |customerDetails           |coverProduct |paymentType  |paymentDetails      |
      |directline |  RM81RR    | Mortgaged,Building and Contents,1975,House | Detached House,Brick,None | 2,2,3,1         |   Nov,13th   | 1,0,0  | prasad sname|Mr,01/01/1969,test@hip.com| HomePlus    |Annually     |4444333322221111,123|
      |privilege  |  RM81RR    | Mortgaged,Building and Contents,1975,House | Detached House,Brick,None | 2,2,3,1         |   Nov,13th   | 0,0,0  | prasad sname|Mr,01/01/1969,test@hip.com| HomePlus    |Annually     |4444333322221111,123|
      |churchill  |  RM81RR    | Mortgaged,Building and Contents,1975,House | Detached House,Brick,None | 2,2,3,1         |   Nov,13th   | 0,0,0  | prasad sname|Mr,01/01/1969,test@hip.com| HomePlus    |Annually     |4444333322221111,123|
      |directline |  RM81RR    | Mortgaged,Building and Contents,1975,House | Detached House,Brick,None | 2,2,3,1         |   Nov,13th   | 1,0,0  | prasad sname|Mr,01/01/1969,test@hip.com| HomePlus    |Monthly      |4444333322221111,123|
      |privilege  |  RM81RR    | Mortgaged,Building and Contents,1975,House | Detached House,Brick,None | 2,2,3,1         |   Nov,13th   | 0,0,0  | prasad sname|Mr,01/01/1969,test@hip.com| HomePlus    |Monthly      |4444333322221111,123|
      |churchill  |  RM81RR    | Mortgaged,Building and Contents,1975,House | Detached House,Brick,None | 2,2,3,1         |   Nov,13th   | 0,0,0  | prasad sname|Mr,01/01/1969,test@hip.com| HomePlus    |Monthly      |4444333322221111,123|


  @wip
  Scenario Outline: Purchage Quote Policy Manager(QPM) Successfully '<brandName>'
    Given I launch home application '<brandName>'
    When enter the '<postCode>','<homeDetails1>','<homeDetails2>','<roomsAndPeople>','<coverStart>','<claims>','<customerName>','<customerDetails>','<paymentType>'
    Then check '<coverProduct>','<paymentType>' and enter the Premium Details
    And clicked on save quote button and fill login Registraion form '<customerDetails>','<paymentType>' and submit
    And review '<customerName>' details and purchase policy using option '<paymentType>' and '<paymentDetails>'
    Then I should get the confirmation for my policy purchase


    Examples:
      |brandName  | postCode  | homeDetails1                               | homeDetails2              | roomsAndPeople | coverStart  | claims  |customerName|customerDetails                 |coverProduct|paymentType|paymentDetails      |
      |directline |  RM81RR   | Mortgaged,Building and Contents,1977,House | Detached House,Brick,None | 2,2,3,1        |   Nov,13th  | 0,0,0  | prasad sname|Mr,01/02/1964,test0009@hip.com  |Home        |Annually   |4444333322221111,123|
      |privilege  |  RM81RR   | Mortgaged,Building and Contents,1977,House | Detached House,Brick,None | 2,2,3,1        |   Nov,13th  | 0,0,0  | prasad sname|Mr,01/02/1972,test0010@hip1.com  |Home        |Annually   |4444333322221111,123|
      |churchill  |  RM81RR   | Mortgaged,Building and Contents,1977,House | Detached House,Brick,None | 2,2,3,1        |   Nov,13th  | 0,0,0  | prasad sname|Mr,01/02/1964,test0509@hip.com  |Home        |Annually   |4444333322221111,123|
      |directline |  RM81RR   | Mortgaged,Building and Contents,1977,House | Detached House,Brick,None | 2,2,3,1        |   Nov,13th  | 0,0,0  | prasad sname|Mr,01/02/1964,test0509@hip.com  |Home        |  Monthly  |4444333322221111,123|
      |privilege  |  RM81RR   | Mortgaged,Building and Contents,1977,House | Detached House,Brick,None | 2,2,3,1        |   Nov,13th  | 0,0,0  | prasad sname|Mr,01/02/1979,test0010@hip.com  |Home        |  Monthly  |4444333322221111,123|
      |churchill  |  RM81RR   | Mortgaged,Building and Contents,1977,House | Detached House,Brick,None | 2,2,3,1        |   Nov,13th  | 0,0,0  | prasad sname|Mr,01/02/1964,test0509@hip.com  |Home        |  Monthly  |4444333322221111,123|




