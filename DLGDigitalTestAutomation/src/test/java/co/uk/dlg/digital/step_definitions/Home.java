package co.uk.dlg.digital.step_definitions;

import co.uk.dlg.digital.BaseStepDef;
import co.uk.dlg.digital.utils.HomeUtil;
import cucumber.api.PendingException;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class Home extends BaseStepDef {

    private HomeUtil homeUtil = new HomeUtil();

    @Given("^I launch home application '(.+)'$")
    public void launchHomeApplication(String brandName) throws Throwable {
        switch (brandName) {
            case "privilege":
                getDriver().manage().deleteAllCookies();
                getDriver().navigate().to(System.getProperty("PR"));
                break;
            case "churchill":
                getDriver().manage().deleteAllCookies();
                getDriver().navigate().to(System.getProperty("CH"));
                break;
            default:
                getDriver().manage().deleteAllCookies();
                getDriver().navigate().to(System.getProperty("DL"));
                break;
        }
    }

    @When("^enter the '(.+)','(.+)','(.+)','(.+)','(.+)','(.+)','(.+)','(.+)','(.+)'$")
    public void userEnterDetails(String postCode, String HomeDetails1, String HomeDetails2, String RoomsAndPeople, String CoverStart, String Claims, String customerName, String customerDetails, String paymentType) throws Throwable {
        homeUtil.fillPostcode(postCode);
        homeUtil.fillHomeDetails(HomeDetails1, HomeDetails2, RoomsAndPeople, CoverStart, Claims);
        homeUtil.fillAboutYourSelf(customerName, customerDetails, paymentType);
    }

    @Then("^check '(.+)','(.+)' and enter the Premium Details$")
    public void userEterPremiumDetails(String coverProduct, String paymentType) {
        homeUtil.enterPremiumDetails(coverProduct, paymentType);
    }

    @And("^review '(.+)' details and purchase policy using option '(.+)' and '(.+)'$")
    public void reviewPaymentDetailsAndBuyNow(String customerName, String paymentType, String paymentDetails) throws Throwable {
        homeUtil.reviewAndConfirm(customerName);
        homeUtil.fillpaymentPage(customerName, paymentType, paymentDetails);
        if (paymentType.equalsIgnoreCase("Annually")) {
            homeUtil.fillSecuryPaymentPage();
        }
    }

    @Then("^I should get the confirmation for my policy purchase$")
    public void confirmationOfPolicyPurchage() throws Throwable {
        homeUtil.verifyConfirmationPage();
    }

    @And("^clicked on save quote button and fill login Registraion form '(.+)','(.+)' and submit$")
    public void userSaveQuoteAndFillLoginRegistraionFormAndSubmit(String customerDetails, String paymentType) throws Throwable {
        homeUtil.saveQuoteAndSubmitRegistration(customerDetails, paymentType);
    }

    @Then("^Validate policy Excess Values and Confirm$")
    public void validatePolicyExcessValuesAndConfirm() throws Throwable {

       homeUtil.validatePolicyMiniumExcessValue();
       homeUtil.validatePolicyDefaultExcessValue();
       //homeUtil.validateContentsSumInsured();

    }
}
